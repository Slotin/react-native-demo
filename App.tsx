import * as React from 'react';
import RootNavigation from './src/navigation/RootNavigation';

type Props = {};
export default class App extends React.Component<Props> {
  render() {
    return (<RootNavigation />);
  }
}