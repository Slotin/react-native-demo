import { Text, TouchableOpacity } from "react-native"
import * as React from "react";
import { StyleSheet } from 'react-native';

interface IProps {
  text: string;
  isDisabled?: boolean;
  onPress(): void;
}

const BottomButton = ( props: IProps) => (
  <TouchableOpacity
    style={[styles.buttonContainer, props.isDisabled ? styles.containerGray : styles.containerRed]}
    onPress={props.onPress}
    activeOpacity={0.8}
  >
    <Text
      style={[styles.buttonText, props.isDisabled ? styles.buttonTextBlack : styles.buttonTextWhite]}
    >{props.text}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    height: '100%'
  },
  containerRed: {
    backgroundColor: 'rgb(255, 57, 0)',
  },
  containerGray: {
    backgroundColor: 'rgb(244, 243, 244)',
  },
  buttonText: {
    marginTop: 20,
    fontSize: 25
  },
  buttonTextWhite: {
    color: 'rgb(255, 235, 229)',
  },
  buttonTextBlack: {
    color: 'black',
  }
})

export default BottomButton;