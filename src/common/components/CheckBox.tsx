import * as React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome5'

interface IProps {
  checked: boolean;
  onPress(): void;
}

export default (props: IProps) => (
  <TouchableOpacity
    style={styles.container}
    onPress={props.onPress}
    activeOpacity={0.8}
  >
      {props.checked ? (<FontAwesome style={styles.icon} name={'check'}/>) : null}
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  container: {
    width: 25,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 8,
    borderStyle: 'solid',
    borderColor: 'rgb(180, 180, 180)'
  },
  icon: {
    color: 'rgb(180, 180, 180)'
  }
})