import { noop } from "lodash";
import * as React from "react";
import { StyleSheet, Text, TouchableOpacity, View} from "react-native";

interface IProps {
  leftButtonIcon: React.ReactElement<any>;
  titleIcon: React.ReactElement<any>;
  isGray?: boolean;
  leftButtonText?: string;
  rightIcon?: React.ReactElement<any>;
  rightIconOnPress?: () => void;

  onLeftButton(): void;
}

class Header extends React.Component<IProps> {
  public renderLeftButtonText = (leftButtonText: string, isGray: boolean) => (
    <Text style={[styles.buttonText, isGray && styles.grayButtonText]}>{leftButtonText}</Text>
  )

  public renderRightIcon = (rightIcon: React.ReactElement<any>, rightIconOnPress?: () => void) => (
    <TouchableOpacity onPress={rightIconOnPress ? rightIconOnPress : noop}>
      {rightIcon}
    </TouchableOpacity>
  )

  public render() {
    const {
      isGray,
      onLeftButton,
      leftButtonIcon,
      leftButtonText,
      titleIcon,
      rightIcon,
      rightIconOnPress,
    } = this.props;
    return (
      <View
        style={[styles.container, isGray ? styles.grayContainer : styles.redContainer]}
      >
        <TouchableOpacity style={styles.leftButton} onPress={onLeftButton}>
          {leftButtonIcon}
          {leftButtonText && this.renderLeftButtonText(leftButtonText, isGray)}
        </TouchableOpacity>
        {titleIcon}
        {rightIcon ? this.renderRightIcon(rightIcon, rightIconOnPress) : (<View style={styles.rightBlock} />)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    width: "100%",
    height: 100,
    paddingBottom: 10,
  },
  redContainer: {
    backgroundColor: "rgb(255, 57, 0)",
  },
  grayContainer: {
    backgroundColor: "rgb(238, 239, 240)",
  },
  leftButton: {
    flexDirection: "row",
    alignItems: "center",
  },
  buttonText: {
    paddingLeft: 5,
    color: "rgb(255, 235, 229)",
    fontSize: 18,
  },
  grayButtonText: {
    color: "rgb(65, 65, 65)",
  },
  rightBlock: {
    width: 85,
    height: 30,
  },
});

export default Header;
