const camera = require("../../assets/doodles/PNG/camera.png");
const freedomPayCard = require("../../assets/images/freedom-pay-card.png");
const logoImage = require("../../assets/logos/FlIcon-gray.png");
const phoneMockup1 = require("../../assets/images/phone-mockup-001.png");
const phoneMockup2 = require("../../assets/images/phone-onboarding-002.png");
const splashScreen = require("../../assets/images/splash-screen-background.png");
const blackLogo = require("../../assets/logos/FlIcon-black.png");

export const ImageSources = {
  doodles: {
    camera,
  },
  images: {
    freedomPayCard,
    logoImage,
    phoneMockup1,
    phoneMockup2,
    splashScreen,
  },
  logos: {
    blackLogo,
  },
};
