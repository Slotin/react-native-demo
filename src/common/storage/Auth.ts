import { AsyncStorage } from "react-native";
import IStorageInterface from "./MainStorageInterface";

const key = "com.FbFreedomPay.someAuthKey";

class AuthStorage implements IStorageInterface {
  public get = () => AsyncStorage.getItem(key);
  public set = (value: string) => AsyncStorage.setItem(key, value);
  public remove = () => AsyncStorage.removeItem(key);
  public isExist = async () => {
    const authKey = await AsyncStorage.getItem(key);
    return !!authKey;
  }
}

const storage = new AuthStorage();

export default storage;
