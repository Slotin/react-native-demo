interface IStorageInterface {
  get(): Promise<string>;
  set(value: string): Promise<void>;
  remove(): Promise<void>;
  isExist(): Promise<boolean>;
}

export default IStorageInterface;
