import { action, observable, runInAction } from "mobx";

import AuthStorage from "../storage/Auth";

class AuthStore {
  @observable
  public email: string = "";

  @observable
  public password: string = "";

  @observable
  public isLoggedIn: boolean = false;

  constructor() {
    // for test purposes
    AuthStorage.remove();
    this.checkIsLoggedIn();
  }

  @action
  public checkIsLoggedIn = async () => {
    const isLoggedIn = await AuthStorage.isExist();
    runInAction(() => {
      this.isLoggedIn = isLoggedIn;
    });
  }

  @action
  public handleChangeEmail = (email: string) => {
    this.email = email;
  }

  @action
  public handleChangePassword = (password: string) => {
    this.password = password;
  }

  @action
  public handleLogin = async () => {
    if (this.email !== "" && this.password !== "") {
      await AuthStorage.set(`${this.email}${this.password}`);
      this.checkIsLoggedIn();
    }
  }
}

const authStore = new AuthStore();

export default authStore;
