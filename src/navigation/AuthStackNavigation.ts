import { createStackNavigator, createAppContainer } from 'react-navigation';

import LoginScreen from '../screens/login';
import CreateAccountInstruction from '../screens/createAccount/CreateAccountInstructions/CreateAccountInstructions.view';
import CreateAccountForm from '../screens/createAccount/AccountForm.view'
import ForgotPassword from '../screens/forgotPassword'

const LoginStackNavigator = createStackNavigator(
  {
    Login: LoginScreen,
    CreateAccountInstruction: CreateAccountInstruction,
    CreateAccountForm: CreateAccountForm,
    ForgotPassword: ForgotPassword
  },
  {
    initialRouteName: 'Login',
    navigationOptions: () => ({
      headerTitleStyle: {
        fontWeight: 'normal',
      },
    }),
  },
);
export default createAppContainer(LoginStackNavigator);