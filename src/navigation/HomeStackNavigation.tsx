import { noop } from "lodash";
import * as React from "react";
import { Image, StyleSheet } from "react-native";
import {
  createAppContainer,
  createBottomTabNavigator,
  createStackNavigator,
  NavigationScreenProp,
} from "react-navigation";

import AccountScreen from "../screens/account/Account.view";
import HomeScreen from "../screens/home";
import PaymentsScreen from "../screens/payments/Payments.view";
import RewardsScreen from "../screens/rewards/Rewards.view";
import SpendingScreen from "../screens/spending/Spending.view";

import Header from "../common/components/Header";
import ActivateAccount from "../screens/activateAccount";
import ImportFunds from "../screens/importFunds";
const logo = require("../assets/logos/FlIcon-black.png");

import Icon from "../common/components/IcoMoonIcons";
import UIStore from "../screens/home/CurrentBalanceScene/UIStore";

const styles = StyleSheet.create({
  focusedColor: {
    color: "rgb(252, 90, 52)",
  },
  leftIcon: {
    color: "rgb(65, 65, 65)",
    fontSize: 30,
    fontWeight: "100",
    paddingLeft: 20,
    paddingRight: 10,
  },
  logo: {
    height: 30,
    width: 30,
  },
  rightIcon: {
    color: "rgb(65, 65, 65)",
    fontSize: 30,
    fontWeight: "100",
    paddingLeft: 10,
    paddingRight: 20,
  },
  tabBarIconStyle: {
    color: "rgb(130, 130, 130)",
    fontSize: 24,
    paddingBottom: 10,
    paddingTop: 10,
  },
});

const HomeNavigator = createBottomTabNavigator(
  {
    Home: {
      navigationOptions: {
        // @ts-ignore
        tabBarIcon: ({ focused }) => (
          <Icon
            style={[styles.tabBarIconStyle, focused && styles.focusedColor]}
            name={"home-black"}
          />),
      },
      screen: HomeScreen,
    },
    Spending: {
      navigationOptions: {
        // @ts-ignore
        tabBarIcon: ({ focused }) => (
          <Icon
            style={[styles.tabBarIconStyle, focused && styles.focusedColor]}
            name={"spending-black"}
          />
        ),
      },
      screen: SpendingScreen,
    },
    Account: {
      navigationOptions: {
        // @ts-ignore
        tabBarIcon: ({ focused }) => (
          <Icon
            style={[styles.tabBarIconStyle, focused && styles.focusedColor]}
            name={"account-black"}
          />),
      },
      screen: AccountScreen,
    },
    Payments: {
      navigationOptions: {
        // @ts-ignore
        tabBarIcon: ({ focused }) => (
          <Icon
            style={[styles.tabBarIconStyle, focused && styles.focusedColor]}
            name={"payments-black"}
          />),
      },
      screen: PaymentsScreen,
    },
    Rewards: {
      navigationOptions: {
        // @ts-ignore
        tabBarIcon: ({ focused }) => (
          <Icon
            style={[styles.tabBarIconStyle, focused && styles.focusedColor]}
            name={"rewards-black"}
          />),
      },
      screen: RewardsScreen,
    },
  }, {
    tabBarOptions: {
      activeTintColor: "rgb(74, 71, 71)",
      inactiveTintColor: "rgb(74, 71, 71)",
      style: {
        height: 55,
      },
    },
  },
);

const HomeContainer = createAppContainer(HomeNavigator);

interface IProps {
  navigation: NavigationScreenProp<any>;
}

class HomeStackWrapper extends React.Component<IProps> {
  // @ts-ignore
  public static navigationOptions = ({ navigation }) => {
    return {
      header: () => (
        <Header
          isGray={true}
          titleIcon={<Image source={logo} style={styles.logo} />}
          leftButtonIcon={(<Icon style={styles.leftIcon} name={"search-black"} />)}
          rightIcon={(<Icon style={styles.leftIcon} name={"location-black"} />)}
          onLeftButton={noop}
        />
      ),
      headerStyle: {
        backgroundColor: "rgb(255, 57, 0)",
      },
      headerTintColor: "rgb(255, 255, 255)",
    };
  }

  constructor(props: IProps) {
    super(props);
    UIStore.setupNavigation(this.props.navigation);
  }
  public render(): React.ReactNode {
    return <HomeContainer />;
  }
}

export const HomeStackNavigator = createStackNavigator({
  Home: HomeStackWrapper,
  ActivateAccount,
  ImportFunds,
});

export default createAppContainer(HomeStackNavigator);
