import * as React from 'react';
import { observer } from 'mobx-react';
import AuthStore from '../common/store/Auth';
import Auth from './AuthStackNavigation';
import Home from './HomeStackNavigation';

@observer
export default class RootNavigation extends React.Component {
  render() {
    return (AuthStore.isLoggedIn ? (<Home />) : (<Auth />));
  }
}