import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  leftIcon: {
    paddingLeft: 10,
    color: "rgb(65, 65, 65)",
    fontSize: 30,
  },
  logo: {
    width: 30,
    height: 30,
  },
  progressContainer: {
    marginTop: 30,
    width: "90%",
    height: 6,
    flexDirection: "row",
  },
  redProgress: {
    width: "10%",
    height: "100%",
    backgroundColor: "rgb(255, 57, 0)",
  },
  grayProgress: {
    width: "90%",
    height: "100%",
    backgroundColor: "rgb(237, 237, 237)",
  },
  smileText: {
    width: "85%",
    fontSize: 18,
    marginTop: 30,
    textAlign: "center",
    color: "rgb(69, 69, 69)",
  },
  redText: {
    color: "rgb(240, 105, 67)",
  },
  photoImg: {
    marginTop: 50,
    width: 180,
    height: 126,
  },
  smallText: {
    width: "80%",
    marginTop: 80,
    fontSize: 12,
  },
  continueButtonWrapper: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: 100,
  },
});
