import { noop } from "lodash";
import * as React from "react";
import { Image, Text, View } from "react-native";

import BottomButton from "../../common/components/BottomButton";
import Header from "../../common/components/Header";
import Icon from "../../common/components/IcoMoonIcons";
import { ImageSources } from "../../common/components/imageResolver";

import styles from "./ActivateAccount.style";

export default class ActivateAccount extends React.Component {
  // @ts-ignore
  public static navigationOptions = ({ navigation }) => {
    const goBackAction = () => navigation.goBack();
    return {
      header: () => (
        <Header
          isGray={true}
          leftButtonIcon={(<Icon style={styles.leftIcon} name={"left-arrow"} />)}
          leftButtonText="Back"
          titleIcon={<Image style={styles.logo} source={ImageSources.logos.blackLogo}/>}
          onLeftButton={goBackAction}
        />
      ),
      headerStyle: {
        backgroundColor: "rgb(255, 57, 0)",
      },
      headerTintColor: "rgb(255, 255, 255)",
    };
  }

  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.progressContainer}>
          <View style={styles.redProgress}/>
          <View style={styles.grayProgress} />
        </View>
        <Text style={styles.smileText}>Are you ready for a selfie? Show us your best 
        <Text style={styles.redText}>smile</Text>!</Text>

        <Image source={ImageSources.doodles.camera} style={styles.photoImg}/>

        <Text style={[styles.smileText, styles.smallText]}>
          * For biometric authentication, we require a realtime motion picture of you.
          Centre your face in the markers and <Text style={styles.redText}>say cheese</Text>!</Text>

        <View style={styles.continueButtonWrapper}>
          <BottomButton
            isDisabled={true}
            onPress={noop}
            text="Continue"
          />
        </View>
      </View>
    );
  }
}
