import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '60%',
    marginTop: 20
  },
  termsContainer: {
    width: '80%',
    marginTop: 80,
    alignItems: 'center'

  },
  termsText: {
    fontSize: 16,
    marginRight: 10
  },
  emailAddressLabel: {
    color: 'rgb(255, 57, 0)'
  },
  emailAddressContainer: {
    marginTop: 60,
    justifyContent: 'flex-start',
    width: '80%'
  },
  textMain: {
    textAlign: 'center',
    color: 'rgb(44, 44, 44)',
    fontSize: 20,
  },
  textRed: {
    fontSize: 20,
    marginLeft: 5,
    color: 'rgb(255, 57, 0)'
  },
  inputContainer: {
    width: '100%',
    height: 60,
    marginBottom: 25,
    position: 'relative'
  },

  inputStyle: {
    marginTop: 10,
    backgroundColor: 'white',
    width: '100%',
    paddingLeft: 10,
    fontSize: 14,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgb(230, 237, 243)',
    borderRadius: 5,
  },
  placeholderStyle: {
    fontStyle: 'italic',
  },
  loginButtonWrapper: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 100
  },
  leftIcon: {
    paddingLeft: 10,
    color: 'rgb(255, 235, 229)',
    fontSize: 30
  },
  logo: {
    width: 30,
    height: 30
  },
})