import { noop } from "lodash";
import { observer } from "mobx-react";
import { Input } from "native-base";
import * as React from "react";
import { Image, Text, View} from "react-native";

import BottomButton from "../../common/components/BottomButton";
import CheckBox from "../../common/components/CheckBox";
import Header from "../../common/components/Header";
import AuthStore from "../../common/store/Auth";
import UIStore from "./UIStore";

const logo = require("../../assets/logos/FlIcon-white.png");

import Icon from "../../common/components/IcoMoonIcons";
import styles from "./AccountForm.styles";

@observer
export default class AccountForm extends React.Component {
  // @ts-ignore
  public static navigationOptions = ({ navigation }) => {
    const onPress = () => navigation.navigate("Login");
    return {
      headerTintColor: "rgb(255, 255, 255)",
      headerStyle: {
        backgroundColor: "rgb(255, 57, 0)",
      },
      header: () => (
        <Header
          leftButtonIcon={(<Icon style={styles.leftIcon} name={"left-arrow"} />)}
          leftButtonText="Back"
          titleIcon={<Image style={styles.logo} source={logo}/>}
          onLeftButton={onPress}
        />
      ),
    };
  }
  public render() {
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.textMain}>Why don"t you tell us a bit about
            <Text style={styles.textRed}> yourself</Text>
            ?
          </Text>
        </View>
        <View style={styles.emailAddressContainer}>
          <Text style={styles.emailAddressLabel}>Email Address</Text>
          <View style={styles.inputContainer}>
            <Input
              style={AuthStore.email.length > 0 ? styles.inputStyle : [styles.inputStyle, styles.placeholderStyle]}
              autoCorrect={false}
              blurOnSubmit={true}
              autoCapitalize="none"
              placeholder="Example patrick@freedompay.co.za"
              value={AuthStore.email}
              placeholderTextColor="rgb(183, 183, 183)"
              onChangeText={AuthStore.handleChangeEmail}
            />
          </View>
        </View>
        <View style={[styles.textContainer, styles.termsContainer]}>
          <Text style={[styles.textMain, styles.termsText]}>I agree to the
            <Text style={[styles.textRed, styles.termsText]}> Terms & Conditions</Text>
          </Text>
          <CheckBox checked={UIStore.termsChecked} onPress={UIStore.handleTermsChecked} />
        </View>
        <View style={styles.loginButtonWrapper}>
          <BottomButton
            isDisabled={UIStore.isContinueDisabled}
            onPress={noop}
            text="Continue"
          />
        </View>
      </View>
    );
  }
}
