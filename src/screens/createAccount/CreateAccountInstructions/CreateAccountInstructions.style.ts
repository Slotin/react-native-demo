import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  tabBar: {
    flexDirection: 'row',
    paddingTop: 30,
    paddingBottom: 30,
    backgroundColor: 'transparent',
    justifyContent: 'center'

  },
  tabItem: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent:'center',
    padding: 0,
  },
  dotActive: {
    height: 10,
    width: 10,
    backgroundColor: 'rgb(255, 57, 0)',
    borderRadius: 20,
  },
  dotNotActive: {
    height: 6,
    width: 6,
    backgroundColor: '#bbb',
    borderRadius: 12,
  },
  textWrapper: {
    marginTop: 80,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 30
  },
  headerTextMain: {
    color: 'rgb(44, 44, 44)',
    fontSize: 20,
  },
  headerTextRed: {
    fontSize: 20,
    marginLeft: 5,
    color: 'rgb(255, 57, 0)'
  },
  redButtonWrapper: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: 100
  }
})