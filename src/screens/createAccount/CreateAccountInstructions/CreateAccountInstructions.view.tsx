import { observer } from "mobx-react";
import * as React from "react";
import { Animated, Text, TouchableOpacity, View } from "react-native";
import { SceneRendererProps, TabView } from "react-native-tab-view";
import { NavigationScreenProp } from "react-navigation";

import BottomButton from "../../../common/components/BottomButton";
import UIStore from "../UIStore";
import styles from "./CreateAccountInstructions.style";

interface IRouteType {
  key: string;
  title: string;
}

interface IProps {
  navigation: NavigationScreenProp<any>;
}

@observer
export default class CreateAccountInstructions extends React.Component<IProps> {
  private static navigationOptions = {
    // @ts-ignore
    header: null,
  };

  public render() {
    const onPress = () => UIStore.handleContinuePress(this.props.navigation);
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderTabView()}
        <View style={styles.redButtonWrapper}>
          <BottomButton
            onPress={onPress}
            text="Continue"
          />
        </View>
      </View>
    );
  }

  private renderHeader = () => (
    <View style={styles.textWrapper}>
      <Text style={styles.headerTextMain}>{UIStore.headerText.main}</Text>
      <Text style={styles.headerTextRed}>{UIStore.headerText.red}</Text>
    </View>
  )

  private renderTabItem = (index: number, props: SceneRendererProps<IRouteType>) => {
    const isActive = index === props.navigationState.index;
    const onPress = () => UIStore.handleIndexChange(index);

    return (
      <TouchableOpacity
        key={index}
        style={styles.tabItem}
        onPress={onPress}
      >
        <Animated.View>
          <View style={isActive ? styles.dotActive : styles.dotNotActive}/>
        </Animated.View>
      </TouchableOpacity>
    );
  }

  private renderTabBar = (props: SceneRendererProps<IRouteType>) => {
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => this.renderTabItem(i, props))}
      </View>
    );
  }

  private renderTabView = () => (
    <TabView
      style={styles.container}
      navigationState={UIStore.navigationState}
      renderScene={UIStore.routesMap}
      renderTabBar={this.renderTabBar}
      onIndexChange={UIStore.handleIndexChange}
    />
  )
}
