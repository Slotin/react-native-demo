import * as React from "react";
import { Image, ImageURISource, StyleSheet, View } from "react-native";

interface IProps {
  img: ImageURISource;
  style?: object;
}

const TabView = (props: IProps) => (
  <View style={styles.tabViewWrapper}>
    <View style={styles.imageWrapper}>
      <Image style={[styles.img, props.style && props.style]} source={props.img}/>
    </View>
  </View>
);

const styles = StyleSheet.create({
  imageWrapper: {
    alignItems: "center",
    height: "90%",
    width: "75%",
  },
  img: {
    height: "100%",
    width: "100%",
  },
  tabViewWrapper: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    height: "100%",
    justifyContent: "flex-end",
    width: "100%",
  },
});

export default TabView;
