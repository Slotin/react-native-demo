import { action, computed, observable } from "mobx";
import * as React from "react";
import { SceneMap } from "react-native-tab-view";
import { NavigationScreenProp } from "react-navigation";

import { ImageSources } from "../../common/components/imageResolver";
import AuthStore from "../../common/store/Auth";
import TabItem from "./CreateAccountInstructions/TabItem.view";

const FirstRoute = () => (<TabItem img={ImageSources.images.phoneMockup1}/>);
const SecondRoute = () => (<TabItem img={ImageSources.images.phoneMockup2}/>);
const thirdRouteStyle = {
  height: 530,
  marginLeft: 70,
  width: 220,
};
const ThirdRoute = () => (
  <TabItem
    img={ImageSources.images.freedomPayCard}
    style={thirdRouteStyle}
  />
);

const headerText = [
  {
    main: "Open an account in",
    red: "minutes",
  }, {
    main: "Invite friends and get",
    red: "rewarded",
  }, {
    main: "Use your card anywhere,",
    red: "anytime",
  },
];

class CreateAccountUiStore {
  @observable
  public navigationState = {
    index: 0,
    routes: [
      { key: "first"},
      { key: "second"},
      { key: "third"},
    ],
  };

  @observable
  public termsChecked: boolean = false;

  @observable
  public routesMap = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });

  @observable
  public showPresentation: boolean = true;

  @computed
  get headerText() {
    return headerText[this.navigationState.index]
  }

  @computed
  get isContinueDisabled() {
    return AuthStore.email.length === 0 || !this.termsChecked;
  }

  @action
  handleIndexChange = (index: number) => {
    const navigationState = this.navigationState;
    this.navigationState = { ...navigationState, index};
  }

  @action
  public handleContinuePress = (navigation: NavigationScreenProp<any>) => {
    const { routes } = this.navigationState;
    let { index } = this.navigationState;
    if (index < routes.length - 1) {
      index++;
      this.handleIndexChange(index);
    } else {
      this.showPresentation = false;
      navigation.navigate("CreateAccountForm");
    }
  }

  @action
  resetShowPresentation = () => {
    this.handleIndexChange(0);
    this.showPresentation = true;
  }

  @action
  handleTermsChecked = () => {
    this.termsChecked = !this.termsChecked;
  }
}

const store = new CreateAccountUiStore();
export default store;