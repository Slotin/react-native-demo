import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  recentTextWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: "rgb(236, 236, 236)",
    borderStyle: "solid",
  },
  recentText: {
    color: "rgb(58, 57, 58)",
    fontSize: 16,
  },
  noRecentWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    paddingTop: 80,
  },
  noRecentText: {
    color: "rgb(58, 57, 58)",
    fontSize: 12,
  },
});
