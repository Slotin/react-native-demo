import * as React from "react";
import { Text, View } from "react-native";

import styles from "./CurrentBalance.style";
import ListItem from "./ListItem";
import UIStore from "./UIStore";

class CurrentBalanceView extends React.Component {
  public renderTabs = () => (
    UIStore.tabs.map((item, index) => (
      <ListItem
        key={index}
        itemData={item}
        onPress={UIStore.navigate}
      />
    ))
  )

  public render() {
    return (
      <View style={styles.container}>
        {this.renderTabs()}
        <View style={styles.recentTextWrapper}>
          <Text style={styles.recentText}>Recent Account Activity</Text>
        </View>
        <View style={styles.noRecentWrapper}>
          <Text style={styles.noRecentText}>* No recent account activity to display</Text>
        </View>
      </View>
    );
  }
}

export default CurrentBalanceView;