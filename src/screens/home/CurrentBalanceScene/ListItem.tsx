import { noop } from "lodash";
import * as React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

import Icon from "../../../common/components/IcoMoonIcons";

import { ITabDescription } from "./types";

interface IProps {
  itemData: ITabDescription;
  onPress?: (routeName: string) => void;
}

export default (props: IProps) => {
  const { isEnabled, iconName, navigationString } = props.itemData;
  return (
    <TouchableOpacity
      style={[styles.container, !isEnabled && styles.disabledContainerBackground]}
      onPress={isEnabled ? () => props.onPress(navigationString) : noop}
      activeOpacity={0.8}
    >
      <Icon
        name={iconName}
        style={[styles.leftIcon, styles.textColor, !isEnabled && styles.disabledLeftIconColor]}
      />
      <View style={styles.textWrapper}>
        <Text
          style={[styles.textColor, styles.mainText, !isEnabled && styles.disabledMainTextColor]}
        >{props.itemData.mainText}
        </Text>
        <Text
          style={[styles.textColor, styles.additionalText, !isEnabled && styles.disabledAdditionalTextColor]}
        >{props.itemData.additionalText}
        </Text>
      </View>
      <Icon
        name={"right-arrow"}
        style={[styles.leftIcon, styles.textColor, styles.rightIcon, !isEnabled && styles.disabledRightIconColor]}
      />
    </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "rgb(236, 236, 236)",
    borderStyle: "solid",
  },
  disabledContainerBackground: {
    backgroundColor: "rgb(247, 248, 248)",
  },
  disabledLeftIconColor: {
    color: "rgb(146, 147, 147)",
  },
  disabledRightIconColor: {
    color: "rgb(171, 172, 172)",
  },
  disabledMainTextColor: {
    color: "rgb(165, 166, 166)",
  },
  disabledAdditionalTextColor: {
    color: "rgb(194, 195, 195)",
  },
  leftIcon: {
    fontSize: 30,
    marginRight: "5%",
    marginLeft: "5%",
  },
  textColor: {
    color: "rgb(57, 57, 57)",
  },
  mainText: {
    fontSize: 16,
  },
  additionalText: {
    fontSize: 10,
    fontWeight: "100",
  },
  rightIcon: {
    fontWeight: "100",
  },
  textWrapper: {
    width: "65%",
  },
});
