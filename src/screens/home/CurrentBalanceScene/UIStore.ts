import { action, observable } from "mobx";

import { NavigationScreenProp } from "react-navigation";
import { ITabDescription } from "./types";

const tabs: ITabDescription[] = [
  {
    iconName: "activate-black",
    mainText: "Activate Account",
    additionalText: "Activate your account to enjoy all the benefits",
    isEnabled: true,
    navigationString: "ActivateAccount",
  }, {
    iconName: "import-funds-black",
    mainText: "Import Funds",
    additionalText: "Import funds to you account to start transacting",
    isEnabled: true,
    navigationString: "ImportFunds",
  }, {
    iconName: "switch-black",
    mainText: "Switch to Freedom Pay",
    additionalText: "Coming Soon",
    isEnabled: false,
    navigationString: "",
  }, {
    iconName: "upgrade-black",
    mainText: "Upgrade your account",
    additionalText: "Coming Soon",
    isEnabled: false,
    navigationString: "",
  }, {
    iconName: "rewards-black",
    mainText: "Get Rewarded",
    additionalText: "Coming Soon",
    isEnabled: false,
    navigationString: "",
  },
];

class UIStore {
  @observable
  public tabs: ITabDescription[] = tabs;

  public navigation: NavigationScreenProp<any>;

  @action
  public setupNavigation = (navigation: NavigationScreenProp<any>) => {
    this.navigation = navigation;
  }

  public navigate = (routeName: string) => {
    if (this.navigation) {
      this.navigation.navigate({ routeName });
    }
  }
}

const store = new UIStore();

export default store;
