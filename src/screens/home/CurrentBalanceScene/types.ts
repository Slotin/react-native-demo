export interface ITabDescription {
  iconName: string;
  mainText: string;
  additionalText: string;
  isEnabled: boolean;
  navigationString: string;
}