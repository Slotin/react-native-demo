import { StyleSheet } from "react-native"

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  tabBarWrapper: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(236, 236, 236)'
  },
  tabBar: {
    flexDirection: 'row',
    paddingTop: 10,
    justifyContent: 'flex-start',
  },
  tabItemWrapper: {
    borderBottomWidth: 5,
    borderStyle: 'solid',
  },
  tabItem: {
    alignItems: 'center',
    padding: 0,
    marginBottom: 10,
  },
  textTabTop: {
    fontSize: 20,
  },
  textTabBottom: {
    fontSize: 8,
  }
});