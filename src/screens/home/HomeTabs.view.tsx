import * as React from 'react';
import {
  View,
  Animated,
  TouchableOpacity,
} from 'react-native';
import { TabView, SceneMap, SceneRendererProps } from 'react-native-tab-view';
import { observer } from "mobx-react";

import styles from './HomeTabs.style';
import CurrentBalanceScene from './CurrentBalanceScene';
import VASScene from './VASScene';
import UIStore, { tabItemWidth } from './UIStore';

type RouteType = {
  key: string;
  title: string;
  description: string;
}


@observer
export default class HomeTabsView extends React.Component {
  renderTabBar = (props: SceneRendererProps<RouteType>) => {
    let { marginLeft, previousMarginLeft } = UIStore;
    if (!marginLeft) {
      marginLeft = UIStore.marginLeftCached;
      previousMarginLeft = marginLeft;
      UIStore.topPanelShift.setValue(1);
    }
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const topPanelShiftValue = UIStore.topPanelShift.interpolate({
      inputRange: [0, 1],
      // @ts-ignore
      outputRange: [previousMarginLeft, marginLeft]
    })

    return (
      <View style={styles.tabBarWrapper}>
        <Animated.View
          style={[styles.tabBar, { marginLeft: topPanelShiftValue }]}
        >
          {props.navigationState.routes.map((route, i) => {
            const colorTop = props.position.interpolate({
              inputRange,
              outputRange: inputRange.map(
                inputIndex => (inputIndex === i ? 'rgb(252, 91, 51)' : 'rgb(214, 214, 214)')
              ),
            });

            const colorBottom = props.position.interpolate({
              inputRange,
              outputRange: inputRange.map(
                inputIndex => (inputIndex === i ? 'rgb(96, 96, 96)' : 'rgb(228, 228, 228)')
              ),
            });

            const borderBottomColor = props.position.interpolate({
              inputRange,
              outputRange: inputRange.map(
                inputIndex => (inputIndex === i ? 'rgb(252, 91, 51)' : '#fff')
              ),
            });
            return (
              <Animated.View
                key={i}
                style={[styles.tabItemWrapper, { borderBottomColor }]}>
                <TouchableOpacity
                  style={[styles.tabItem, { width: tabItemWidth }]}
                  onPress={() => UIStore.updateNavigationStateIndex(i)}>
                  <Animated.Text style={[ styles.textTabTop, { color: colorTop }]}>{route.title}</Animated.Text>
                  <Animated.Text style={[styles.textTabBottom, { color: colorBottom }]}>{route.description}</Animated.Text>
                </TouchableOpacity>
              </Animated.View>
            );
          })}
        </Animated.View>
      </View>
    );
  };

  renderScene = SceneMap({
    first: CurrentBalanceScene,
    second: VASScene,
  });

  render() {
    return (
      <TabView
        navigationState={UIStore.navigationState}
        renderScene={this.renderScene}
        renderTabBar={(props) => this.renderTabBar(props)}
        onIndexChange={UIStore.updateNavigationStateIndex}
      />
    );
  }
}
