import { observable, action, computed } from "mobx";
import { Animated, Dimensions, Easing } from "react-native"

const { width } = Dimensions.get('window');

export const tabItemWidth = 120;

class HomeTabsUIStore {
  @observable
  navigationState = {
    index: 0,
    routes: [
      { key: 'first', title: 'R 0', description: 'Current Balance' },
      { key: 'second', title: 'V.A.S', description: 'Value Added Service' }
    ],
  }

  @computed
  get marginLeftCached() {
    return ((width/2) - (tabItemWidth/2));
  }

  @action
  calculateMargin = (index: number) => {
    let { marginLeft } = this;
    if (!marginLeft) {
      marginLeft = this.marginLeftCached;
    }
    this.topPanelShift.setValue(0);
    this.previousMarginLeft = marginLeft;
    this.marginLeft = (width/2 - (tabItemWidth/2)) - (tabItemWidth * index);

    Animated.timing(this.topPanelShift, {
      toValue: 1,
      duration: 100,
      easing: Easing.linear
    }).start();
  }

  @observable
  marginLeft: Number = null;

  @observable
  previousMarginLeft: Number = null;

  @action
  updateNavigationStateIndex = (index: number) => {
    const { navigationState } = this;

    this.navigationState = { ...navigationState, index };
    this.calculateMargin(index);
  }

  topPanelShift: Animated.Value;

  constructor() {
    this.topPanelShift = new Animated.Value(0);
  }
}

const store = new HomeTabsUIStore();
export default store;