import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  // main container styles
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '100%'
  },
  topContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center'
  },
  bottomContainer: {
    width: '100%',
    height: 200,
    justifyContent: 'flex-end',
  },
  scrollContainer: {
    flex: 1
  },
  scrollContentContainer: {
    height: '100%'
  },
  backgroundImageStyle: {
    width: '100%',
    height: '100%'
  },

  // logo styles
  loginButtonWrapper: {
    width: '100%',
    height: '50%'
  },
  logoIcon: {
    width: 50,
    height: 50,
    marginRight: 10
  },
  logoContainer: {
    marginTop: 80,
    flexDirection: 'row',
    alignItems: 'flex-start'
  },
  logoTextContainer: {
    height: 90,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  logoTextMain: {
    color: 'rgb(142, 142, 142)',
    fontSize: 50,
    lineHeight: 50,
    fontWeight: 'bold',
    paddingBottom: 0,
    marginBottom: 0,
  },
  logoTextAdditional: {
    fontSize: 35,
    fontWeight: 'normal',
    lineHeight: 30,
    position: 'relative',
    top: -5
  },

  // input styles
  credentialsContainer: {
    width: '70%',
    marginTop: 50,
  },
  inputContainer: {
    width: '100%',
    height: 50,
    marginBottom: 25,
    position: 'relative'
  },

  inputStyle: {
    backgroundColor: 'white',
    width: '100%',
    paddingLeft: 10,
    fontSize: 12,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgb(230, 237, 243)',
    borderRadius: 5,
  },
  forgotContainer: {
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    right: 10,
    bottom: 0
  },
  forgotText: {
    fontSize: 12,
    color: 'rgb(255, 57, 0)'
  },

  // create account styles
  createAccountContainer: {
    height: '50%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  createAccountButton: {
    width: '70%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgba(230, 237, 243, 0)',
    borderRadius: 5,
    fontSize: 12,
  },
  createAccountText: {
    color: 'black'
  }
});

export default styles;