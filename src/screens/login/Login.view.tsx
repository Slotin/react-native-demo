import { observer } from "mobx-react";
import { Input } from "native-base";
import * as React from "react";
import {
  Image,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { NavigationScreenProp } from "react-navigation";

import BottomButton from "../../common/components/BottomButton";
import { ImageSources } from "../../common/components/imageResolver";
import AuthStore from "../../common/store/Auth";

import CreateAccountUiStore from "../createAccount/UIStore";

import styles from "./Login.style";

interface IProps {
  navigation: NavigationScreenProp<any>;
}

@observer
export default class LoginView extends React.Component<IProps> {
  public static navigationOptions = {
    // @ts-ignore
    header: null,
  };

  public render() {
    const navigateToForgotPassword = () => this.props.navigation.navigate("ForgotPassword");
    return (
      <View style={styles.container}>
        <ImageBackground source={ImageSources.images.splashScreen} style={styles.backgroundImageStyle}>
          <KeyboardAwareScrollView
            style={styles.scrollContainer}
            contentContainerStyle={styles.scrollContentContainer}
          >
            <View style={styles.container}>
              <View style={styles.topContainer}>
                <View style={styles.logoContainer}>
                  <Image source={ImageSources.images.logoImage} style={styles.logoIcon}/>
                  <View style={styles.logoTextContainer}>
                    <Text style={styles.logoTextMain}>Freedom</Text>
                    <Text
                      style={[styles.logoTextMain, styles.logoTextAdditional]}
                    >
                      pay
                    </Text>
                  </View>
                </View>
                <View style={styles.credentialsContainer}>
                  <View style={styles.inputContainer}>
                    <Input
                      style={styles.inputStyle}
                      autoCorrect={false}
                      blurOnSubmit={true}
                      autoCapitalize="none"
                      placeholder="Email"
                      placeholderTextColor="#000"
                      value={AuthStore.email}
                      onChangeText={AuthStore.handleChangeEmail}
                    />
                  </View>
                  <View style={styles.inputContainer}>
                    <Input
                      style={styles.inputStyle}
                      autoCorrect={false}
                      blurOnSubmit={true}
                      autoCapitalize="none"
                      secureTextEntry={true}
                      placeholder="Password"
                      placeholderTextColor="#000"
                      value={AuthStore.password}
                      onChangeText={AuthStore.handleChangePassword}
                    />
                    <TouchableOpacity
                      style={styles.forgotContainer}
                      onPress={navigateToForgotPassword}
                    >
                      <Text style={styles.forgotText}>Forgot?</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={styles.bottomContainer}>
                <View style={styles.createAccountContainer}>
                  <TouchableOpacity
                    style={styles.createAccountButton}
                    onPress={this.goToCreateAccount}
                  >
                    <Text style={styles.createAccountText}>Create an Account</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.loginButtonWrapper}>
                  <BottomButton
                    onPress={AuthStore.handleLogin}
                    text="Log in"
                  />
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }

  private goToCreateAccount = () => {
    const route = CreateAccountUiStore.showPresentation ? "CreateAccountInstruction" : "CreateAccountForm";
    this.props.navigation.navigate(route);
  }
}
